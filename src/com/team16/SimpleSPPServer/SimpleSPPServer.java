package com.team16.SimpleSPPServer;

//import java.io.BufferedReader;
import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import javax.bluetooth.*;
import javax.microedition.io.*;
  
/**
* Class that implements an SPP Server which accepts single line of
* message from an SPP client and sends a single line of response to the client.
*/
public class SimpleSPPServer {
    
    //start server
    private void startServer() throws IOException{
    	
    	
        //Create a UUID for SPP
        UUID uuid = new UUID("fa87c0d0afac11de8a390800200c9a66",false);
        //Create the servicve url
        String connectionString = "btspp://localhost:" + uuid +";name=Sample SPP Server";
        
        //open server url
        StreamConnectionNotifier streamConnNotifier = (StreamConnectionNotifier)Connector.open( connectionString );
        
        //Wait for client connection
        System.out.println("\nServer Started. Waiting for clients to connect...");
        StreamConnection connection=streamConnNotifier.acceptAndOpen();
  
        RemoteDevice dev = RemoteDevice.getRemoteDevice(connection);
        System.out.println("Remote device address: "+dev.getBluetoothAddress());
        System.out.println("Remote device name: "+dev.getFriendlyName(true));
        
        String temp = null;
        double temp1 = 0;
        boolean running = true;
        int count = 0;
        int max = 500;
        OutputStream outStream=connection.openOutputStream();
        PrintWriter pWriter=new PrintWriter(new OutputStreamWriter(outStream));
        while(running) {
        	temp1 = CPU.getCpuUsage()*100;
        	temp = Double.toString(temp1);//+"%";
        	System.out.println("Random generate sequence: "+temp);
        	//send response to spp client
        
        	pWriter.write(temp);
        	pWriter.flush();
        	//pWriter.close();
        
        	try {
        		Thread.sleep(250L);
        		}	
        		catch (Exception exception) {
        			exception.printStackTrace();
        			}
        
        
        
        	if(count++ == max) {
        		running = false;
        		}
        
        	}
        
        
        streamConnNotifier.close();
        
  
    }
  
  
    public static void main(String[] args) throws IOException {
        
        //display local device address and name
        LocalDevice localDevice = LocalDevice.getLocalDevice();
        System.out.println("Address: "+localDevice.getBluetoothAddress());
        System.out.println("Name: "+localDevice.getFriendlyName());
        
        SimpleSPPServer sampleSPPServer=new SimpleSPPServer();
        sampleSPPServer.startServer();
        
    }
}