package com.team16.SimpleSPPServer;

import java.util.*;

/**
 * Class that generates a Random String of numbers and characters
 * @author Angelo
 *
 */
public class RandomGeneration{
    /**
     * @param args the command line arguments
     */
    public static String main(String args) {
    	int n = Integer.parseInt(args);
       return randomString(n);
        
    }
    
    /**
     * @param lenght of the string generated
     */
    public static String randomString(int length){
        Random rand = new Random();
        StringBuffer tempStr = new StringBuffer();
        tempStr.append("");
        for (int i = 0; i < length; i++) {
            int  c = rand.nextInt(122 - 48) + 48;
            if((c >= 58 &&  c <= 64) || (c >= 91 && c <= 96)){
                i--;
                continue;
            }
            tempStr.append((char)c);
            
        }
        return tempStr.toString();
    }
    
    
}