package com.team16.SimpleSPPServer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import javax.bluetooth.*;
import javax.microedition.io.*;
  
/**
* Class that implements an SPP Server which sends multiple line of response to the client.
*/
public class DataSimulator {
    
	/**
	 * start server and wait for a connection
	 * @throws IOException
	 */
    private void startServer() throws IOException{
    	
        //Create a UUID for SPP
        UUID uuid = new UUID("0000110100001000800000805F9B34FB",false);
        //Create the servicve url
        String connectionString = "btspp://localhost:" + uuid +";name=Sample SPP Server";
        
        //open server url
        StreamConnectionNotifier streamConnNotifier = (StreamConnectionNotifier)Connector.open( connectionString );
        
        //Wait for client connection
        System.out.println("\nServer Started. Waiting for clients to connect...");
        StreamConnection connection=streamConnNotifier.acceptAndOpen();
  
        RemoteDevice dev = RemoteDevice.getRemoteDevice(connection);
        System.out.println("Remote device address: "+dev.getBluetoothAddress());
        System.out.println("Remote device name: "+dev.getFriendlyName(true));
        
        String temp = null;
        double temp1 = 0;
        boolean running = true;
        int count = 0;
        int max = 1000000;
        OutputStream outStream=connection.openOutputStream();
        PrintWriter pWriter=new PrintWriter(new OutputStreamWriter(outStream));
        while(running) {
        	temp1 = CPU.getCpuUsage()*100;
        	temp = Double.toString(temp1);
        	System.out.println("Random generate sequence: "+temp);
        	
        	//send response to spp client
        	pWriter.write(temp);
        	pWriter.flush();
        	try {
        		Thread.sleep(250L);
        		}	
        		catch (Exception exception) {
        			exception.printStackTrace();
        			}
        	if(count++ == max) {
        		running = false;
        		}
        
        	}
        streamConnNotifier.close();
    }
  
    /**
     * getting the local Bluetooth adaptor and launching the startServer() method
     * @param args nothing
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        //display local device address and name
        LocalDevice localDevice = LocalDevice.getLocalDevice();
        System.out.println("Address: "+localDevice.getBluetoothAddress());
        System.out.println("Name: "+localDevice.getFriendlyName());
        DataSimulator dataSimulator=new DataSimulator();
        dataSimulator.startServer();
    }
}