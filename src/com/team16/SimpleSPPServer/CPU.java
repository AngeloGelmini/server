package com.team16.SimpleSPPServer;


import java.lang.management.*;
import com.sun.management.OperatingSystemMXBean;  


/**
 * Class that is used to return the current value of the CPU Load
 * @author Angelo
 *
 */
public class CPU {

	/**
	 * 
	 * @return the CPU Load
	 */
    public synchronized static double getCpuUsage()
    {
    	OperatingSystemMXBean operatingSystemMXBean = (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
    	return operatingSystemMXBean.getSystemCpuLoad();
    }
}

